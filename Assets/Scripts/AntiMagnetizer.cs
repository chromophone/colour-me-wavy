﻿using UnityEngine;

namespace Assets.Scripts
{
    public class AntiMagnetizer : FunctionalObject
    {
        [SerializeField]
        private static readonly Color AntiMagnetizerColor = new Color(0.901961f, 0, 1f);

        public AntiMagnetizer() : base(AntiMagnetizerColor)
        {
        }

        void Start()
        {
            base.Start();
        }

        protected override float GetTargetGravityScale()
        {
            return -1f;
        }
    }
}
