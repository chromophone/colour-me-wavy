﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    public class Checkpoint : MonoBehaviour
    {


        private CheckpointController checkpointController;

        private void Start()
        {
            checkpointController = CheckpointController.Instance;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.tag == "Player")
            {
                checkpointController.LastCheckpointPosition = transform.position;

                checkpointController.LastCheckpointFunction = other.gameObject.GetComponent<FunctionalObject>().GetType();
            }

        }
    }
}
