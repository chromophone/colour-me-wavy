﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    public class SoundFxController : MonoBehaviour
    {

        //public vars
        public static SoundFxController Instance { get { return _instance; } }

        public audio_script music;

        public AudioSource SoundFxSource;
        public AudioClip Bounce;
        public AudioClip Jump;
        public AudioClip Absorb;
        public AudioClip Die;

        public bool playBackgroundMusic = true; //channel 1
        public bool playBlueMusic = false; //channel 2
        public bool playRedMusic = false; //channel 3
        public bool playYellowMusic = false; //channel 4
        public bool playPurpleMusic = false; //channel 5

        
        //private vars

        private static SoundFxController _instance;

        



        void Awake()
        {
            if (_instance != null && _instance != this)
            {
                Destroy(this.gameObject);
            }
            else
            {
                _instance = this;
            }
        }

        private void Update()
        {
            if (playBackgroundMusic)
            {
                music.channel_1 = true;
            }

        if (playBlueMusic)
            {
                music.channel_2 = true;
                music.channel_3 = false;
                music.channel_4 = false;
                music.channel_5 = false;
            }
        if (playRedMusic)
            {
                music.channel_2 = false;
                music.channel_3 = true;
                music.channel_4 = false;
                music.channel_5 = false;
            }
        if (playYellowMusic)
            {
                music.channel_2 = false;
                music.channel_3 = false;
                music.channel_4 = true;
                music.channel_5 = false;
            }
        if (playPurpleMusic)
            {
                music.channel_2 = false;
                music.channel_3 = false;
                music.channel_4 = false;
                music.channel_5 = true;
            }
        }

        public void SwitchMusic(Type type)
        {
            //Debug.Log(functionalObjectType);
            if (type == typeof(Shrinker))
            {
                music.channel_2 = true;
                music.channel_3 = false;
                music.channel_4 = false;
                music.channel_5 = false;
            }

            if (type == typeof(Accelerator))
            {
                music.channel_2 = false;
                music.channel_3 = true;
                music.channel_4 = false;
                music.channel_5 = false;
            }

            if (type == typeof(Bouncer))
            {
                music.channel_2 = false;
                music.channel_3 = false;
                music.channel_4 = true;
                music.channel_5 = false;
            }

            if (type == typeof(StandardGrayFunctional))
            {
                music.channel_2 = false;
                music.channel_3 = false;
                music.channel_4 = false;
                music.channel_5 = false;
            }
            if (type == typeof(AntiMagnetizer))
            {
                music.channel_2 = false;
                music.channel_3 = false;
                music.channel_4 = false;
                music.channel_5 = true;
            }
        }

    }

}
