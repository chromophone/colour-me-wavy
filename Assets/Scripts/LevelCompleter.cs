﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts
{
    public class LevelCompleter : MonoBehaviour {

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.tag == "Player")
            {
                SceneManager.LoadScene("MainMenu");
            }
        }
    }
}
