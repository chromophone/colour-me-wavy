﻿using UnityEngine;

namespace Assets.Scripts
{
    public class Shrinker : FunctionalObject {

        [SerializeField]
        private static readonly Color ShrinkColor = new Color(0, 0.2941177f, 1f);

        [SerializeField] private const float ShrinkSize = .5f;

        public Shrinker() : base(ShrinkColor)
        {
        
        }

        protected override float GetTargetSize()
        {
            return ShrinkSize;
        }
    
        void Start () {
            base.Start();
        }
    }
}
