﻿using UnityEngine;

namespace Assets.Scripts
{
    public class Player : MonoBehaviour
    {

        private CheckpointController checkpointController;
        private SoundFxController soundFxController;
        private Rigidbody2D _rigidbody;
        
        public void Die()
        {
            checkpointController.ResetAllColoredObjects();

            soundFxController.SoundFxSource.PlayOneShot(soundFxController.Die, .6f);

            Debug.Log("You died!!!");
            
            transform.position = checkpointController.LastCheckpointPosition;
            _rigidbody.velocity = Vector2.zero;
            
            Destroy(GetComponent<FunctionalObject>());
            gameObject.AddComponent(checkpointController.LastCheckpointFunction);
            soundFxController.SwitchMusic(checkpointController.LastCheckpointFunction);
        }

        private void Start()
        {
            checkpointController = CheckpointController.Instance;
            soundFxController = SoundFxController.Instance;
            _rigidbody = GetComponent<Rigidbody2D>();

        }

        private void Update()
        {
            if (transform.position.y > 25 || transform.position.y < -25)
            {
                Die();
            }
        }
    }
}
