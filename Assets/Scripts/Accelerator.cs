﻿using UnityEngine;

namespace Assets.Scripts
{
    public class Accelerator : FunctionalObject
    {
        [SerializeField]
        private static readonly Color AcceleratorColor = new Color(1f, 0.09412f, 0);

        private float StartXPosition;

        private bool _isPlayer;

        public Accelerator() : base(AcceleratorColor)
        {
        }

        void Start()
        {
            base.Start();

            _isPlayer = GetComponent<PlayerMovement>() != null;
            StartXPosition = transform.position.x;
        }

        protected override float GetTargetAcceleration()
        {
            return 10f;
        }

        private void Update()
        {
            if (!_isPlayer)
            {
                transform.position = new Vector3(StartXPosition, transform.position.y) + Vector3.right*(Mathf.PerlinNoise(Random.value, 0) - 0.5f);
            }
        }
    }
}
