﻿using System;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts
{
    public abstract class FunctionalObject : MonoBehaviour {
        [SerializeField]
        private const float SwitchDuration = 0.2f;

        private readonly Color _objectColor;

        private SpriteRenderer _spriteRenderer;

        protected FunctionalObject(Color color)
        {
            _objectColor = color;
        }

        protected void Start()
        {
            _spriteRenderer = gameObject.GetComponentInChildren<SpriteRenderer>();
            StartCoroutine(ChangeColor());
            StartCoroutine(ShrinkToCorrectSize());
            StartCoroutine(GoToCorrectBounciness());
            StartCoroutine(GoToCorrectHorizontalAcceleration());
            StartCoroutine(GoToCorrectGravityScale());
        }

        protected virtual float GetTargetSize()
        {
            return 1f;
        }

        protected virtual float GetTargetBounciness()
        {
            return 0f;
        }

        protected virtual float GetTargetAcceleration()
        {
            return 5f;
        }

        protected virtual float GetTargetGravityScale()
        {
            return 1f;
        }

        private IEnumerator ChangeColor()
        {
            float timer = 0f;       

            while (timer <= SwitchDuration)
            {
                timer += Time.deltaTime;
                float lerpPercentage = timer / SwitchDuration;

                _spriteRenderer.color = Color.Lerp(_spriteRenderer.color, _objectColor, lerpPercentage);

                yield return null;
            }
        }

        private IEnumerator ShrinkToCorrectSize()
        {
            float timer = 0f;

            while (timer <= SwitchDuration)
            {
                timer += Time.deltaTime;
                float lerpPercentage = timer / SwitchDuration;

                var endScale = new Vector3(Math.Sign(transform.localScale.x), Math.Sign(transform.localScale.y), Math.Sign(transform.localScale.z)) * GetTargetSize();
                transform.localScale = Vector2.Lerp(transform.localScale, endScale, lerpPercentage);
                yield return null;
            }
        }

        private IEnumerator GoToCorrectBounciness()
        {
            float timer = 0f;
            var inBetweenMaterial = (PhysicsMaterial2D)Resources.Load("PhysicMaterials/InBetweenBouncy");
            var bouncinessCollider = gameObject.GetComponent<Collider2D>();
            bouncinessCollider.sharedMaterial = inBetweenMaterial;
            while (timer <= SwitchDuration)
            {
                timer += Time.deltaTime;
                float lerpPercentage = timer / SwitchDuration;

                inBetweenMaterial.bounciness = Mathf.Lerp(inBetweenMaterial.bounciness, GetTargetBounciness(), lerpPercentage);
                yield return null;
            }
            bouncinessCollider.sharedMaterial = GetTargetBounciness() == 1f ? (PhysicsMaterial2D)Resources.Load("PhysicMaterials/Bouncy") : (PhysicsMaterial2D)Resources.Load("PhysicMaterials/NotBouncy");

            if(GetTargetBounciness() == 1f && GetComponent<PlayerMovement>() == null) GetComponent<Rigidbody2D>().AddForce(Vector2.up * 300f);
        }

        private IEnumerator GoToCorrectHorizontalAcceleration()
        {
            float timer = 0f;
            var playerMovement = GetComponent<PlayerMovement>();
            while (playerMovement != null && timer <= SwitchDuration)
            {
                timer += Time.deltaTime;
                float lerpPercentage = timer / SwitchDuration;

                playerMovement.HorizontalVelocityAmount = Mathf.Lerp(playerMovement.HorizontalVelocityAmount, GetTargetAcceleration(), lerpPercentage);
                yield return null;
            }
        }

        private IEnumerator GoToCorrectGravityScale()
        {
            float timer = 0f;
            var rigidbody = GetComponent<Rigidbody2D>();

            transform.rotation = Quaternion.Euler(0f, 0f, GetTargetGravityScale() == -1f? 180f : 0f);
            transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x) * Mathf.Sign(GetTargetGravityScale()), transform.localScale.y, transform.localScale.z);

            while (timer <= SwitchDuration)
            {
                timer += Time.deltaTime;
                float lerpPercentage = timer / SwitchDuration;

                rigidbody.gravityScale = Mathf.Lerp(rigidbody.gravityScale, GetTargetGravityScale(), lerpPercentage);
                yield return null;
            }
        }
    }
}
