﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuGoer : MonoBehaviour {
    
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("MainMenu");
        }
	}
}
