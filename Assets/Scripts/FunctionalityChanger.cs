﻿using System;
using UnityEngine;

namespace Assets.Scripts
{
    public class FunctionalityChanger : MonoBehaviour {

        private SoundFxController _soundFxController;

        private void OnTriggerStay2D(Collider2D collision)
        {
            if (collision.gameObject.tag != "Player") return;
            if (!Input.GetButtonDown("Fire1")) return;

           
            _soundFxController.SoundFxSource.PlayOneShot(_soundFxController.Absorb, 1f);

            var coloredObject = gameObject.transform.parent.gameObject;
            var functionalObject = coloredObject.GetComponent<FunctionalObject>();
            var playerColoredObject = collision.gameObject.GetComponent<FunctionalObject>();

            var triggerType = functionalObject.GetType();
            var playerType = playerColoredObject.GetType();

            //SwitchMusic(triggerType);
            _soundFxController.SwitchMusic(triggerType);

            Destroy(functionalObject);
            Destroy(playerColoredObject);

            coloredObject.AddComponent(playerType);
            collision.gameObject.AddComponent(triggerType);
        }

        private void Start()
        {
            _soundFxController = SoundFxController.Instance;
        }

        private void SwitchMusic(Type functionalObjectType)
        {
            //Debug.Log(functionalObjectType);
            if (functionalObjectType == typeof(Shrinker)){
                _soundFxController.playBlueMusic = true;
                _soundFxController.playRedMusic = false;
                _soundFxController.playYellowMusic = false;
            }

            if (functionalObjectType == typeof(Accelerator)){
                _soundFxController.playBlueMusic = false;
                _soundFxController.playRedMusic = true;
                _soundFxController.playYellowMusic = false;
            }

            if (functionalObjectType == typeof(Bouncer))
            {
                _soundFxController.playBlueMusic = false;
                _soundFxController.playRedMusic = false;
                _soundFxController.playYellowMusic = true;
            }

            if (functionalObjectType == typeof(StandardGrayFunctional))
            {
                _soundFxController.playBlueMusic = false;
                _soundFxController.playRedMusic = false;
                _soundFxController.playYellowMusic = false;
            }
        }
    }
}
