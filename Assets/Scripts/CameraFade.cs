﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class CameraFade : MonoBehaviour
    {
        public float FadeTime;

        public IEnumerator FadeOutCoroutine()
        {
            float timer = 0f;

            while (timer <= FadeTime)
            {
                timer += Time.deltaTime;
                float lerpPercentage = timer / FadeTime;

                var fadeImage = GetComponent<Image>();
                fadeImage.color = Color.Lerp(new Color(0f, 0f, 0f, 0f), new Color(0f, 0f, 0f, 1f), lerpPercentage);

                yield return null;
            }
        }
    }
}
