﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    [SerializeField]
    private GameObject player;
    private Vector3 targetPosition;
    

    void LateUpdate () {
        targetPosition = player.transform.position;
        targetPosition.z = transform.position.z;
        targetPosition.y = transform.position.y;

        transform.position = targetPosition;
	}
}
