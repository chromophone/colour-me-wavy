﻿using UnityEngine;

namespace Assets.Scripts
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class PlayerMovement : MonoBehaviour
    {

        public float HorizontalVelocityAmount = 2f;

        [SerializeField]
        private float _jumpForce = 10f;

        [SerializeField]
        private LayerMask _collisionMask;

        [SerializeField]
        private Animator _animator;

        private float _playerDirection = 1;
        private Rigidbody2D _playerRigidbody;
        private float _directionalInput;

        private float _distToGround;

        private SoundFxController _soundFxController;

        void Start()
        {
            _playerRigidbody = GetComponent<Rigidbody2D>();
            _distToGround = GetComponent<Collider2D>().bounds.extents.y;
            _soundFxController = SoundFxController.Instance;
        }

        void Update()
        {

            GetInput();
            Move();
            CalculateAnimationState();
            CheckPlayerBounce();

            GetComponentInChildren<SpriteRenderer>().flipX = (_playerDirection == 1) ? false : true;
        }

        void CheckPlayerBounce()
        {
            if (IsGrounded() &&  _playerRigidbody.velocity.y > 0f && GetComponent<Bouncer>())
            {
                //player has bouncer functionality and hit the floor this frame
                _soundFxController.SoundFxSource.PlayOneShot(_soundFxController.Bounce, .6f);
            }
        }

        void CalculateAnimationState()
        {
            var gravityScale = Mathf.Sign(transform.localScale.x);
            if (_playerRigidbody.velocity.y * gravityScale > 0)
            {
                _animator.SetBool("Falling", false);
                _animator.SetBool("Jumping", true);
            }
            else if (_playerRigidbody.velocity.y * gravityScale < 0)
            {
                _animator.SetBool("Jumping", false);
                _animator.SetBool("Falling", true);
            }

            if (IsGrounded() || _playerRigidbody.velocity.y == 0)
            {
                _animator.SetBool("Jumping", false);
                _animator.SetBool("Falling", false);
            }
        }

        void GetInput()
        {

            bool playerIsBouncy = (GetComponent<Bouncer>()) ? true : false;
            _directionalInput = Input.GetAxis("Horizontal");
            CalculatePlayerDirection();

            if (Input.GetButtonDown("Jump") && IsGrounded())
            {
                Jump();                
            }

            //if (Input.GetButton("Jump") && IsGrounded() && playerIsBouncy)
            //{
            //    IncreaseBouncyness();
            //}
            

        }
        void IncreaseBouncyness()
        {
            var gravityScale = Mathf.Sign(transform.localScale.x);
            _playerRigidbody.AddForce(Vector2.up * gravityScale * _jumpForce * 1.2f);
        }

        void CalculatePlayerDirection()
        {
            if (_directionalInput != 0)
            {
                _playerDirection = Mathf.Sign(_directionalInput);
            }
        }

        bool IsGrounded()
        {
            var gravityScale = Mathf.Sign(transform.localScale.x);
            RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, (_distToGround + .2f) * gravityScale, _collisionMask);

            return hit.collider != null;
        }

        void Move()
        {
            _playerRigidbody.velocity = new Vector2(_directionalInput * HorizontalVelocityAmount, _playerRigidbody.velocity.y);

            if (_directionalInput != 0 && _animator.GetBool("Jumping") == false && _animator.GetBool("Falling") == false)
            {
                _animator.SetBool("Walking", true);
            }
            else
            {
                _animator.SetBool("Walking", false);
            }
        }

        void Jump()
        {
            _soundFxController.SoundFxSource.PlayOneShot(_soundFxController.Jump, .6f);
            var playerHasShrinker = transform.localScale.x < 1f;
            var gravityScale = Mathf.Sign(transform.localScale.x);
            if (playerHasShrinker)
            {
                _playerRigidbody.AddForce(Vector2.up * gravityScale * _jumpForce * 0.75f);
            }
            else
            {
                _playerRigidbody.AddForce(Vector2.up * gravityScale * _jumpForce);
            }
        }
    }
}
