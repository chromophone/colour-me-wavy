﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts
{
    public class Stomper : MonoBehaviour
    {
        public float StompTimeOffset = 0.5f;

        private float _stompInterval = 1.1f;
        private float _stompAmplitude = 3f;
        
        void Start()
        {
            Invoke("DelayedStompStart", StompTimeOffset);
        }

        private void DelayedStompStart()
        {
            StartCoroutine(MoveDown());
        }

        private IEnumerator MoveDown()
        {
            float timer = 0f;
            var endPosition = transform.position + (transform.rotation.z == 0f ? Vector3.down : Vector3.up) *_stompAmplitude;
            while (timer <= _stompInterval)
            {
                timer += Time.deltaTime;
                float lerpPercentage = timer / _stompInterval;

                transform.position = Vector2.Lerp(transform.position, endPosition, lerpPercentage);
                yield return null;
            }
            StartCoroutine(MoveUp());
        }

        private IEnumerator MoveUp()
        {
            float timer = 0f;
            var endPosition = transform.position + (transform.rotation.z == 0f ? Vector3.up : Vector3.down) * _stompAmplitude;
            while (timer <= _stompInterval)
            {
                timer += Time.deltaTime;
                float lerpPercentage = timer / _stompInterval;

                transform.position = Vector2.Lerp(transform.position, endPosition, lerpPercentage);
                yield return null;
            }
            StartCoroutine(MoveDown());
        }
    }
}
