﻿using System;
using UnityEngine;

namespace Assets.Scripts
{
    public class CheckpointController : MonoBehaviour
    {
        [SerializeField]
        private Transform startPosition;

        [SerializeField]
        private GameObject ColoredObjectsParent;

        private static CheckpointController instance;
        private Vector2 lastCheckpointPosition = Vector2.zero;

        private Type lastCheckpointFunction;


        public static CheckpointController Instance { get { return instance; } }

        public Type LastCheckpointFunction
        {
            get
            {
                if (lastCheckpointFunction != null)
                {
                    return lastCheckpointFunction;
                } else
                {
                    return typeof(StandardGrayFunctional);
                }
            }
            set { lastCheckpointFunction = value; }
        }

        public Vector2 LastCheckpointPosition
        {
            get
            {
                if (lastCheckpointPosition != Vector2.zero)
                {
                    return lastCheckpointPosition;
                }
                else
                {
                    return startPosition.position;
                }

            }
            set { lastCheckpointPosition = value; }
        }
        
        void Awake()
        {
            if (instance != null && instance != this)
            {
                Destroy(this.gameObject);
            }
            else
            {
                instance = this;
            }
        }

        public void ResetAllColoredObjects()
        {
            foreach (var coloredObjectResetter in ColoredObjectsParent.GetComponentsInChildren<ColoredObjectResetter>())
            {
                coloredObjectResetter.Reset();
            }
        }
    }
}
