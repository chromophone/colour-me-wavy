﻿using UnityEngine;

namespace Assets.Scripts
{
    public class DamagePlayer : MonoBehaviour {

        [SerializeField]
        private Player _player;

        private void OnTriggerEnter2D(Collider2D other)
        {
            _player.Die();
        }
    }
}
