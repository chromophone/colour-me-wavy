﻿using UnityEngine;

namespace Assets.Scripts
{
    public class StandardGrayFunctional : FunctionalObject
    {
        private static readonly Color TargetColor = Color.white;

        public StandardGrayFunctional() : base(TargetColor)
        {
        }
    }
}
