﻿using System;
using UnityEngine;

namespace Assets.Scripts
{
    public class ColoredObjectResetter : MonoBehaviour
    {
        private Type _startType;

        void Start ()
        {
            _startType = GetComponent<FunctionalObject>().GetType();
        }

        public void Reset()
        {
            Destroy(GetComponent<FunctionalObject>());
            gameObject.AddComponent(_startType);
        }

    }
}
