﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Assets.Scripts
{
    public class SceneChangeHandler : MonoBehaviour
    {
        public float FadeTime;
        public Image FadeImage;

        void Start()
        {
            StartCoroutine(FadeInCoroutine());
        }

        public void ChangeScene(string sceneName)
        {
            StartCoroutine(FadeOutCoroutine(sceneName));
        }

        public IEnumerator FadeOutCoroutine(string sceneName)
        {
            float timer = 0f;

            while (timer <= FadeTime)
            {
                timer += Time.deltaTime;
                float lerpPercentage = timer / FadeTime;

                FadeImage.color = Color.Lerp(new Color(0f, 0f, 0f, 0f), new Color(0f, 0f, 0f, 1f), lerpPercentage);

                yield return null;
            }
            SceneManager.LoadScene(sceneName);
        }

        public IEnumerator FadeInCoroutine()
        {
            float timer = 0f;

            while (timer <= FadeTime)
            {
                timer += Time.deltaTime;
                float lerpPercentage = timer / FadeTime;

                FadeImage.color = Color.Lerp(new Color(0f, 0f, 0f, 1f), new Color(0f, 0f, 0f, 0f), lerpPercentage);

                yield return null;
            }
        }

        public void QuitGame()
        {
            Application.Quit();
        }
    }
}
