﻿using UnityEngine;

namespace Assets.Scripts
{
    public class Bouncer : FunctionalObject
    {
        [SerializeField]
        private static readonly Color BouncerColor = new Color(0.941176f, 1f, 0);

        public Bouncer() : base(BouncerColor)
        {
        }

        void Start()
        {
            base.Start();
        }

        protected override float GetTargetBounciness()
        {
            return 1f;
        }
    }
}
