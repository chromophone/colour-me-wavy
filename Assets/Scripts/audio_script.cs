﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class audio_script : MonoBehaviour {


	public AudioMixer audio_mixer;
	public float pitch_shift = 0.001f;
	public float default_pitch = 1.0f;

	public bool channel_1;
	public bool channel_2;
	public bool channel_3;
	public bool channel_4;
	public bool channel_5;
	public bool stop;

	public bool hits;
	public int random_hits;

	public float fadeout_speed = 10.0f;
	public float fadein_speed = 30.0f;

	private AudioSource audio_channel_1;
	private AudioSource audio_channel_2;
	private AudioSource audio_channel_3;
	private AudioSource audio_channel_4;
	private AudioSource audio_channel_5;
	private AudioSource audio_hits;



	private float audio_channel_1_vol = -80.0f;
	private float audio_channel_2_vol = -80.0f;
	private float audio_channel_3_vol = -80.0f;
	private float audio_channel_4_vol = -80.0f;
	private float audio_channel_5_vol = -80.0f;

	private Object[] AudioArray_channel_1;
	private Object[] AudioArray_channel_2;
	private Object[] AudioArray_channel_3;
	private Object[] AudioArray_channel_4;
	private Object[] AudioArray_channel_5;
	private Object[] AudioArray_hits;

	private float timer_initialize = 2.0f;
	private float timer_volume;



	// Use this for initialization
	void Start () {
		pitch_shift = 0.001f;
		audio_channel_1 = (AudioSource)gameObject.AddComponent <AudioSource>();
		audio_channel_2 = (AudioSource)gameObject.AddComponent <AudioSource>();
		audio_channel_3 = (AudioSource)gameObject.AddComponent <AudioSource>();
		audio_channel_4 = (AudioSource)gameObject.AddComponent <AudioSource>();
		audio_channel_5 = (AudioSource)gameObject.AddComponent <AudioSource>();
		audio_hits = (AudioSource)gameObject.AddComponent <AudioSource>();

		AudioArray_channel_1 = Resources.LoadAll("1",typeof(AudioClip));
		AudioArray_channel_2 = Resources.LoadAll("2",typeof(AudioClip));
		AudioArray_channel_3 = Resources.LoadAll("3",typeof(AudioClip));
		AudioArray_channel_4 = Resources.LoadAll("4",typeof(AudioClip));
		AudioArray_channel_5 = Resources.LoadAll("5",typeof(AudioClip));
		AudioArray_hits = Resources.LoadAll("hits",typeof(AudioClip));

		audio_channel_1.outputAudioMixerGroup = audio_mixer.FindMatchingGroups("channel_1")[0];
		audio_channel_2.outputAudioMixerGroup = audio_mixer.FindMatchingGroups("channel_2")[0];
		audio_channel_3.outputAudioMixerGroup = audio_mixer.FindMatchingGroups("channel_3")[0];
		audio_channel_4.outputAudioMixerGroup = audio_mixer.FindMatchingGroups("channel_4")[0];
		audio_channel_5.outputAudioMixerGroup = audio_mixer.FindMatchingGroups("channel_5")[0];

		audio_channel_1.clip = AudioArray_channel_1[0] as AudioClip;
		audio_channel_2.clip = AudioArray_channel_2[0] as AudioClip;
		audio_channel_3.clip = AudioArray_channel_3[0] as AudioClip;
		audio_channel_4.clip = AudioArray_channel_4[0] as AudioClip;
		audio_channel_5.clip = AudioArray_channel_5[0] as AudioClip;

		audio_channel_1.loop = true;
		audio_channel_2.loop = true;
		audio_channel_3.loop = true;
		audio_channel_4.loop = true;
		audio_channel_5.loop = true;

		audio_channel_1.Play();
		audio_channel_2.Play();
		audio_channel_3.Play();
		audio_channel_4.Play();
		audio_channel_5.Play();

	}
	
	// Update is called once per frame
	void Update () {
		SetVolumes ();
		if (stop) {
			StopAllMusic ();
		}
		if (hits) {
			random_hits = Random.Range (0, 4);
			audio_hits.clip = AudioArray_hits[random_hits] as AudioClip;

			audio_hits.PlayOneShot (audio_hits.clip, 1.0f);
			hits = false;
		}
	}

	public void StopAllMusic(){
		channel_1 = false;
		channel_2 = false;
		channel_3 = false;
		channel_4 = false;
		channel_5 = false;
		if (audio_channel_1_vol < -79.0f) {
			audio_channel_1.Stop ();
		}
		if (audio_channel_2_vol < -79.0f) {
			audio_channel_2.Stop ();
		}
		if (audio_channel_3_vol < -79.0f) {
			audio_channel_3.Stop ();
		}
		if (audio_channel_4_vol < -79.0f) {
			audio_channel_4.Stop ();
		}
		if (audio_channel_5_vol < -79.0f) {
			audio_channel_5.Stop ();
		}

	}

	public void SetVolumes(){
		audio_mixer.SetFloat ("channel_1", audio_channel_1_vol);
		audio_mixer.SetFloat ("channel_2", audio_channel_2_vol);
		audio_mixer.SetFloat ("channel_3", audio_channel_3_vol);
		audio_mixer.SetFloat ("channel_4", audio_channel_4_vol);
		audio_mixer.SetFloat ("channel_5", audio_channel_5_vol);

		if (channel_1) {
			if (audio_channel_1_vol < 0.0f) {
				audio_channel_1_vol += fadein_speed * Time.deltaTime;	
			}
		}
		if (!channel_1) {
			if (audio_channel_1_vol > -80.0f) {
				audio_channel_1_vol -= fadeout_speed * Time.deltaTime;	
			}
		}

		if (channel_2) {
			if (audio_channel_2_vol < 0.0f) {
				audio_channel_2_vol += fadein_speed * Time.deltaTime;	
			}
		}
		if (!channel_2) {
			if (audio_channel_2_vol > -80.0f) {
				audio_channel_2_vol -= fadeout_speed * Time.deltaTime;	
			}
		}
		if (channel_3) {
			if (audio_channel_1.pitch < 1.1f) {
				audio_channel_1.pitch = audio_channel_1.pitch + pitch_shift;
			}
			if (audio_channel_2.pitch < 1.1f) {
				audio_channel_2.pitch = audio_channel_2.pitch + pitch_shift;
			}
			if (audio_channel_3.pitch < 1.1f) {
				audio_channel_3.pitch = audio_channel_3.pitch + pitch_shift;
			}
			if (audio_channel_4.pitch < 1.1f) {
				audio_channel_4.pitch = audio_channel_4.pitch + pitch_shift;
			}

			if (audio_channel_3_vol < 0.0f) {
				audio_channel_3_vol += fadein_speed * Time.deltaTime;	
			}
		}
		if (!channel_3) {
			if (audio_channel_1.pitch > 1.0f) {
				audio_channel_1.pitch = audio_channel_1.pitch - pitch_shift;
			}
			if (audio_channel_2.pitch > 1.0f) {
				audio_channel_2.pitch = audio_channel_2.pitch - pitch_shift;
			}
			if (audio_channel_3.pitch > 1.0f) {
				audio_channel_3.pitch = audio_channel_3.pitch - pitch_shift;
			}
			if (audio_channel_4.pitch > 1.0f) {
				audio_channel_4.pitch = audio_channel_4.pitch - pitch_shift;
			}


			if (audio_channel_3_vol > -80.0f) {
				audio_channel_3_vol -= fadeout_speed * Time.deltaTime;	
			}
		}
		if (channel_4) {
			if (audio_channel_4_vol < 0.0f) {
				audio_channel_4_vol += fadein_speed * Time.deltaTime;	
			}
		}
		if (!channel_4) {
			if (audio_channel_4_vol > -80.0f) {
				audio_channel_4_vol -= fadeout_speed * Time.deltaTime;	
			}
		}
		if (channel_5) {
			if (audio_channel_5_vol < 0.0f) {
				audio_channel_5_vol += fadein_speed * Time.deltaTime;	
			}
		}
		if (!channel_5) {
			if (audio_channel_5_vol > -80.0f) {
				audio_channel_5_vol -= fadeout_speed * Time.deltaTime;	
			}
		}

	}

	public void StopMusic(){
		stop = true;
	}

	public void SetChannel (int channel_number){
		if (channel_number == 2) {
			channel_2 = true;
			channel_3 = false;
			channel_4 = false;
			channel_5 = false;
		}
		if (channel_number == 3) {
			channel_2 = false;
			channel_3 = true;
			channel_4 = false;
			channel_5 = false;
		}
		if (channel_number == 4) {
			channel_2 = false;
			channel_3 = false;
			channel_4 = true;
			channel_5 = false;
		}
		if (channel_number == 5) {
			channel_2 = false;
			channel_3 = false;
			channel_4 = false;
			channel_5 = true;
		}
	}

}
